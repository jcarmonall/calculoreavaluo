package cl.sii.sdi.lob.bbrr.simulacion.controller;

import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import cl.sii.sdi.lob.bbrr.simulacion.facade.ServiciosFacade;
import cl.sii.sdi.lob.bbrr.simulacion.model.EacEstudio;
import cl.sii.sdi.lob.bbrr.simulacion.model.ListEacEstudio;

@Controller
@RequestMapping("/simulacion/listEac")
public class JSONListEacEstudio {
	
	@RequestMapping(method = RequestMethod.GET)
	
	public @ResponseBody
	List<ListEacEstudio>  getJSONListEacEstudio() {
		System.out.println("Aquí Voy JSONListEacEstudio" );
		ServiciosFacade facade = new ServiciosFacade();
		System.out.println(facade.getListEacEstudio().toString());
		return facade.getListEacEstudio();
	}
}