/**
 * 
 */
package cl.sii.sdi.lob.bbrr.simulacion.model;

import java.util.List;

/**
 * @author kperez
 *
 */
public class EacEstudio {
	
	private float eac_estudio_id;
	private int codigo;
	private String serie;
	private String descripcion;
	private String estado;
	private String fCrea;
	private String fAct;
	private String rutCrea;
	private String rutAct;	

	
	public EacEstudio(float eac_estudio_id, int codigo, String serie,
			String descripcion, String estado, String fCrea, String fAct,
			String rutCrea, String rutAct) {
		super();
		this.eac_estudio_id = eac_estudio_id;
		this.codigo = codigo;
		this.serie = serie;
		this.descripcion = descripcion;
		this.estado = estado;
		this.fCrea = fCrea;
		this.fAct = fAct;
		this.rutCrea = rutCrea;
		this.rutAct = rutAct;
	}
	
	
	public float getEac_estudio_id() {
		return eac_estudio_id;
	}
	public void setEac_estudio_id(float eac_estudio_id) {
		this.eac_estudio_id = eac_estudio_id;
	}
	public int getCodigo() {
		return codigo;
	}
	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}
	public String getSerie() {
		return serie;
	}
	public void setSerie(String serie) {
		this.serie = serie;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}
	public String getfCrea() {
		return fCrea;
	}
	public void setfCrea(String fCrea) {
		this.fCrea = fCrea;
	}
	public String getfAct() {
		return fAct;
	}
	public void setfAct(String fAct) {
		this.fAct = fAct;
	}
	public String getRutCrea() {
		return rutCrea;
	}
	public void setRutCrea(String rutCrea) {
		this.rutCrea = rutCrea;
	}
	public String getRutAct() {
		return rutAct;
	}
	public void setRutAct(String rutAct) {
		this.rutAct = rutAct;
	}
	
	
	
	
	

}
