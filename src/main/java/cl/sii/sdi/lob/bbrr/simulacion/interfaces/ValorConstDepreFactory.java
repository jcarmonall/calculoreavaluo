/**
 * 
 */
package cl.sii.sdi.lob.bbrr.simulacion.interfaces;


import org.springframework.stereotype.Component;
import cl.sii.sdi.lob.bbrr.simulacion.model.ValorConstDepre;

/**
 * @author developer
 *
 */
@Component("valorConstDepreFactory")
public class ValorConstDepreFactory {


  public ValorConstDepre create(float valorContDepreID, 
		  float eacEstudioId, 
		  String clase, 
		  int calidad,
		  String descripcion,
		  double tasaDep,
		  double porcMaxDep,
		  double valorRef,
		  double valorProp, 
		  String fCrea, 
		  String fAct,
		  String rutCrea, 
		  String rutAct) {
      return new ValorConstDepre(valorContDepreID,
    		  eacEstudioId, 
    		  clase, 
    		  calidad, 
    		  descripcion, 
    		  tasaDep, 
    		  porcMaxDep, 
    		  valorRef, 
    		  valorProp, 
    		  fCrea, 
    		  fAct, 
    		  rutCrea, 
    		  rutAct);	
  }

}
