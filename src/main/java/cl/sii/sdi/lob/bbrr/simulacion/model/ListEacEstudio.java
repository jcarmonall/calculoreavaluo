/**
 * 
 */
package cl.sii.sdi.lob.bbrr.simulacion.model;

/**
 * @author kperez
 *
 */
public class ListEacEstudio {
	
	private float eacEstudioID;
	private String codDesc;

	/**
	 * 
	 */
	public ListEacEstudio() {
		// TODO Auto-generated constructor stub
	}
	
	

	public ListEacEstudio(float eacEstudioID, String codDesc) {
		super();
		this.eacEstudioID = eacEstudioID;
		this.codDesc = codDesc;
	}



	public float getEacEstudioID() {
		return eacEstudioID;
	}

	public void setEacEstudioID(float eacEstudioID) {
		this.eacEstudioID = eacEstudioID;
	}

	public String getCodDesc() {
		return codDesc;
	}

	public void setCodDesc(String codDesc) {
		this.codDesc = codDesc;
	}



	@Override
	public String toString() {
		return "ListEacEstudio [eacEstudioID=" + eacEstudioID + ", codDesc="
				+ codDesc + "]";
	}
	
	
	
	

}
