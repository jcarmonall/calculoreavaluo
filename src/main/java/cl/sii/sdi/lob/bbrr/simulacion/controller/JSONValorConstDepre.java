package cl.sii.sdi.lob.bbrr.simulacion.controller;

import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import cl.sii.sdi.lob.bbrr.simulacion.facade.ServiciosFacade;
import cl.sii.sdi.lob.bbrr.simulacion.model.ValorConstDepre;


@Controller
@RequestMapping("/simulacion/valorConstDep")
public class JSONValorConstDepre {
	@RequestMapping(method = RequestMethod.GET)
	public @ResponseBody
	List<ValorConstDepre> getValoresJSON() {
		
		ServiciosFacade facade = new ServiciosFacade();

		return  facade.getAllValores();

	}

}
