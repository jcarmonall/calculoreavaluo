/**
 * 
 */
package cl.sii.sdi.lob.bbrr.simulacion.model;

import java.sql.Timestamp;



/**
 * @author developer
 *
 */
public class ValorConstDepre {
	
	private float valorContDepreID;
	private float eacEstudioId;
	private String clase;
	private int calidad;
	private String descripcion;
	private double tasaDep;
	private double porcMaxDep;
	private double valorRef;
	private double valorProp;
	private String fCrea;
	private String fAct;
	private String rutCrea;
	private String rutAct;
	
	public ValorConstDepre(float valorContDepreID, float eacEstudioId,
			String clase, int calidad, String descripcion, double tasaDep,
			double porcMaxDep, double valorRef, double valorProp,
			String fCrea, String fAct, String rutCrea, String rutAct) {
		super();
		this.valorContDepreID = valorContDepreID;
		this.eacEstudioId = eacEstudioId;
		this.clase = clase;
		this.calidad = calidad;
		this.descripcion = descripcion;
		this.tasaDep = tasaDep;
		this.porcMaxDep = porcMaxDep;
		this.valorRef = valorRef;
		this.valorProp = valorProp;
		this.fCrea = fCrea;
		this.fAct = fAct;
		this.rutCrea = rutCrea;
		this.rutAct = rutAct;
	}








	public float getValorContDepreID() {
		return valorContDepreID;
	}



	public void setValorContDepreID(float valorContDepreID) {
		this.valorContDepreID = valorContDepreID;
	}



	public float getEacEstudioId() {
		return eacEstudioId;
	}



	public void setEacEstudioId(float eacEstudioId) {
		this.eacEstudioId = eacEstudioId;
	}



	public String getClase() {
		return clase;
	}



	public void setClase(String clase) {
		this.clase = clase;
	}



	public int getCalidad() {
		return calidad;
	}



	public void setCalidad(int calidad) {
		this.calidad = calidad;
	}



	public String getDescripcion() {
		return descripcion;
	}



	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}



	public double getTasaDep() {
		return tasaDep;
	}



	public void setTasaDep(double tasaDep) {
		this.tasaDep = tasaDep;
	}



	public double getPorcMaxDep() {
		return porcMaxDep;
	}



	public void setPorcMaxDep(double porcMaxDep) {
		this.porcMaxDep = porcMaxDep;
	}



	public double getValorRef() {
		return valorRef;
	}



	public void setValorRef(double valorRef) {
		this.valorRef = valorRef;
	}



	public double getValorProp() {
		return valorProp;
	}



	public void setValorProp(double valorProp) {
		this.valorProp = valorProp;
	}



	public String getfCrea() {
		return fCrea;
	}



	public void setfCrea(String fCrea) {
		this.fCrea = fCrea;
	}



	public String getfAct() {
		return fAct;
	}



	public void setfAct(String fAct) {
		this.fAct = fAct;
	}



	public String getRutCrea() {
		return rutCrea;
	}



	public void setRutCrea(String rutCrea) {
		this.rutCrea = rutCrea;
	}



	public String getRutAct() {
		return rutAct;
	}



	public void setRutAct(String rutAct) {
		this.rutAct = rutAct;
	}
	
	

}
