package cl.sii.sdi.lob.bbrr.simulacion.services;

import cl.sii.sdi.lob.bbrr.simulacion.interfaces.IFactorComunas;
import cl.sii.sdi.lob.bbrr.simulacion.model.Comuna;

public class FactorComunasImpl implements IFactorComunas{

	@Override
	public Comuna getAllComunas() {
		Comuna comuna = new Comuna();
		comuna.setIdComuna(13010);
		comuna.setNomComuna("Prueba");
		comuna.setFactor(12);
		comuna.setGrupo(1);

		return comuna;
	}

}
