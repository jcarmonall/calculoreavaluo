package cl.sii.sdi.lob.bbrr.simulacion.facade;

import java.util.List;

import cl.sii.lob.bbr.simulacion.dao.EacEstudioDaoJdbc;
import cl.sii.lob.bbr.simulacion.dao.ValorConstDepreDaoJdbc;
import cl.sii.sdi.lob.bbrr.simulacion.model.Comuna;
import cl.sii.sdi.lob.bbrr.simulacion.model.EacEstudio;
import cl.sii.sdi.lob.bbrr.simulacion.model.ListEacEstudio;
import cl.sii.sdi.lob.bbrr.simulacion.model.ValorConstDepre;
import cl.sii.sdi.lob.bbrr.simulacion.services.EacEstudioImp;
import cl.sii.sdi.lob.bbrr.simulacion.services.FactorComunasImpl;



public class ServiciosFacade {
	FactorComunasImpl factor = new FactorComunasImpl();
	ValorConstDepreDaoJdbc vcdDao = new ValorConstDepreDaoJdbc();
	EacEstudioDaoJdbc eacDao = new EacEstudioDaoJdbc();
	EacEstudioImp eacEstudioImp = new EacEstudioImp();
	

	
	public Comuna getAllComunas(){
		return factor.getAllComunas();
	}
	
	public List<ValorConstDepre> getAllValores(){
		return vcdDao.getValoresConstDepre();
	}
	
	public List<ListEacEstudio>  getListEacEstudio(){
		return eacEstudioImp.getListEacEstudio();
	}
	

	

}
