package cl.sii.sdi.lob.bbrr.simulacion.interfaces;

import cl.sii.sdi.lob.bbrr.simulacion.model.Comuna;

public interface IFactorComunas {
	public Comuna getAllComunas();
}
