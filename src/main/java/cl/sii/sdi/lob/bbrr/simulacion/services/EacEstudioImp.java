/**
 * 
 */
package cl.sii.sdi.lob.bbrr.simulacion.services;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import cl.sii.lob.bbr.simulacion.dao.EacEstudioDaoJdbc;
import cl.sii.sdi.lob.bbrr.simulacion.model.EacEstudio;
import cl.sii.sdi.lob.bbrr.simulacion.model.ListEacEstudio;

/**
 * @author kperez
 *
 */
public class EacEstudioImp {

	public EacEstudioImp() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	
	public List<ListEacEstudio>  getListEacEstudio(){
		List<ListEacEstudio> listEac = new ArrayList<ListEacEstudio>();
		EacEstudioDaoJdbc eacDao = new EacEstudioDaoJdbc();
		
		for(EacEstudio e : eacDao.getListEacEstudio()) {
            listEac.add(new ListEacEstudio(e.getEac_estudio_id(), e.getCodigo()+" - "+e.getDescripcion()));
        }
		return listEac;
	}

}
