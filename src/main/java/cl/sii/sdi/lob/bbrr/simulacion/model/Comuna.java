package cl.sii.sdi.lob.bbrr.simulacion.model;

public class Comuna {
	
	private int idComuna;
	private String nomComuna;
	private int grupo;
	private int factor;
	public int getIdComuna() {
		return idComuna;
	}
	public void setIdComuna(int idComuna) {
		this.idComuna = idComuna;
	}
	public String getNomComuna() {
		return nomComuna;
	}
	public void setNomComuna(String nomComuna) {
		this.nomComuna = nomComuna;
	}
	public int getGrupo() {
		return grupo;
	}
	public void setGrupo(int grupo) {
		this.grupo = grupo;
	}
	public int getFactor() {
		return factor;
	}
	public void setFactor(int factor) {
		this.factor = factor;
	}
	
	
	

}
