/**
 * 
 */
package cl.sii.sdi.lob.bbrr.simulacion.interfaces;

import org.springframework.stereotype.Component;

import cl.sii.sdi.lob.bbrr.simulacion.model.EacEstudio;

/**
 * @author kperez
 *
 */
@Component("valorConstDepreFactory")
public class EacEstudioFactory {
	public EacEstudio create(float eac_estudio_id, 
			int codigo, 
			String serie,
			String descripcion, 
			String estado, 
			String fCrea, 
			String fAct,
			String rutCrea,
			String rutAct){
		return new EacEstudio(eac_estudio_id, codigo, serie, descripcion, estado, fCrea, fAct, rutCrea, rutAct);
		
	}

}
