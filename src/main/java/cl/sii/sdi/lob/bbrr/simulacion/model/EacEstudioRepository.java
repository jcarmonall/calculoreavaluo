/**
 * 
 */
package cl.sii.sdi.lob.bbrr.simulacion.model;

import java.util.List;

/**
 * @author kperez
 *
 */
public interface EacEstudioRepository {
	
	public EacEstudio getEacEstudio(float id);
    public List<EacEstudio> getListEacEstudio();
    public EacEstudio createEacEstudio(EacEstudio valorConstDepre);
    public EacEstudio updateEacEstudio(EacEstudio valorConstDepre);

}
