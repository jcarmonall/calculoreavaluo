/**
 * 
 */
package cl.sii.sdi.lob.bbrr.simulacion.model;

import java.util.List;

/**
 * @author developer
 *
 */
public interface ValorConstDepreRepository {
	public ValorConstDepre getValorConstDepre(float id);
    public List<ValorConstDepre> getValoresConstDepre();
    public ValorConstDepre createValorConstDepre(ValorConstDepre valorConstDepre);
    public ValorConstDepre updateValorConstDepre(ValorConstDepre valorConstDepre);

}
