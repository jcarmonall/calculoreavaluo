package cl.sii.lob.bbr.simulacion.data.api.model;

public interface ModelResponse<T> {
    void setData(T data);

    T getData();

    void setMetaData(ResponseMetaData responseMetaData);

    ResponseMetaData getMetaData();
}
