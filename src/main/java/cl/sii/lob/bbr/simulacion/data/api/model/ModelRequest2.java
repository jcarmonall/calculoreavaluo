package cl.sii.lob.bbr.simulacion.data.api.model;

public interface ModelRequest2<T> {
    void setData(T data);

    T getData();

    void setMetaData(RequestMetaData2 requestMetaData);

    RequestMetaData2 getMetaData();
}
