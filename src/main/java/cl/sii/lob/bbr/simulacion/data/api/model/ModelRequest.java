package cl.sii.lob.bbr.simulacion.data.api.model;

public interface ModelRequest<T> {
    void setData(T data);

    T getData();

    void setMetaData(RequestMetaData requestMetaData);

    RequestMetaData getMetaData();
}
