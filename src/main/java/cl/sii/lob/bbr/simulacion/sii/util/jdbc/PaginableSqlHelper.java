package cl.sii.lob.bbr.simulacion.sii.util.jdbc;

import java.util.List;

//@Component("paginadorSqlHelper")
public class PaginableSqlHelper<T> {
	
	private String sqlSelect;
	private int pageIndex;
	private int pageSize;
	private int fullSize;
	
	private List<T> listPage;
	
	public PaginableSqlHelper(){
	}
	
	public PaginableSqlHelper(String sqlSelect, int pageIndex, int pageSize){
		this.sqlSelect = sqlSelect;
		this.pageIndex = pageIndex;
		this.pageSize = pageSize;
	}
	
	public void setFullSize(int fullSize){
		this.fullSize = fullSize;
	}
	
	public int getFullSize(){
		return this.fullSize;
	}
	
		
	public int getLastPageIndex(){
		int resto = this.fullSize % this.pageSize;
		int lastPageIndex = 0;
		if(resto == 0)
			lastPageIndex = (this.fullSize / this.pageSize);
		else 
			lastPageIndex = Math.round(this.fullSize / this.pageSize) + 1;
		
		return lastPageIndex;
	}
	
	public int getInitRow(){
		int result = ((this.pageSize * this.pageIndex) - this.pageSize) + 1 ;
		return result;
	}
	
	public int getEndRow(){
		int result = this.pageSize * this.pageIndex;
		if(result > this.fullSize){
			result = this.fullSize;
		}
		return result;
	}
	
	public String getSqlByPage(){
		StringBuffer buff = new StringBuffer(100);
		buff.append("SELECT * FROM ( SELECT /*+ FIRST_ROWS(n) */ A.*, ROWNUM RNUM FROM ( ");
		buff.append(sqlSelect);
		buff.append(" ) A WHERE ROWNUM <= " );
		buff.append(this.getEndRow());
		buff.append(" ) WHERE RNUM  >=  ");
		buff.append(this.getInitRow());
		
		return buff.toString();
	}
	
	public String getSqlCount(){
		StringBuffer buff = new StringBuffer(100);
		buff.append("select count(1) from ( ");
		buff.append(this.sqlSelect);
		buff.append(" ) t1 ");
		return buff.toString();
	}

	public List<T> getListPage() {
		return listPage;
	}

	public void setListPage(List<T> listPage) {
		this.listPage = listPage;
	}

	public int getPageIndex() {
		return pageIndex;
	}

	public void setPageIndex(int pageIndex) {
		this.pageIndex = pageIndex;
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public void setSqlSelect(String sqlSelect) {
		this.sqlSelect = sqlSelect;
	}
	
}

