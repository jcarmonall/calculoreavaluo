/**
 * 
 */
package cl.sii.lob.bbr.simulacion.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.joda.time.DateTimeZone;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import cl.sii.lob.bbr.simulacion.sii.util.jdbc.SqlLoaderHelper;
import cl.sii.lob.bbr.simulacion.util.exception.CommonHelper;
import cl.sii.lob.bbr.simulacion.util.exception.DomainException;
import cl.sii.lob.bbr.simulacion.util.exception.DomainValidator;
import cl.sii.lob.bbr.simulacion.util.exception.DomainException.Type;
import cl.sii.sdi.lob.bbrr.simulacion.interfaces.EacEstudioFactory;
import cl.sii.sdi.lob.bbrr.simulacion.model.EacEstudio;
import cl.sii.sdi.lob.bbrr.simulacion.model.EacEstudioRepository;
import cl.sii.sdi.lob.bbrr.simulacion.model.ValorConstDepre;


/**
 * @author kperez
 *
 */
@Repository("EacEstudioDaoRepository")
public class EacEstudioDaoJdbc implements EacEstudioRepository {
	private SqlLoaderHelper sqls = new SqlLoaderHelper(this.getClass());
	private JdbcTemplate jdbcTemplate;
	private EacEstudioFactory eacEstudioFactory;

	@Autowired
	@Qualifier("EacEstudioDaoRepository")
	public void setDataSource(DataSource dataSource) {
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}

	@Autowired
	@Qualifier("EacEstudioDaoFactory")
	public void setMessageFactory(EacEstudioFactory eacEstudioFactory) {
		this.eacEstudioFactory = eacEstudioFactory;
	}

	@Override
	public EacEstudio getEacEstudio(float id) {
		final String namespace = CommonHelper.createNamespace(getClass(), "getEacEstudio");
		final String errTechMsg = "Valor de Construcción y Depreciación no se pueden cargar";	
		return new EacEstudio(1, 1, "A", "Data Prueba A", "A", "28/09/2016", "28/09/2016", "11810994-0",  "11810994-0");
		
	/*
		try {
			return jdbcTemplate.queryForObject(sqls.getSql("getEacEstudio"), new Object[]{id}, new RowMapper<EacEstudio>() {
			@Override
			public EacEstudio mapRow(ResultSet rs, int index) throws SQLException {
				
			// Validamos que el resultset tenga registros
			float eac_estudio_id = rs.getFloat("EAC_ESTUDIO_ID");
			int codigo = rs.getInt("CODIGO");
			String serie = rs.getString("SERIE");
			String descripcion = rs.getString("DESCRIPCION");
			String estado = rs.getString("ESTADO");
			String fCrea = rs.getString("F_CREA");
			String fAct = rs.getString("F_ACT");
			String rutCrea = rs.getString("RUT_CREA");
			String rutAct = rs.getString("RUT_ACT");
			
			EacEstudio eacEstudio = eacEstudioFactory.create(eac_estudio_id, codigo, serie, descripcion, estado, fCrea, fAct, rutCrea, rutAct );
				
			DomainValidator.Factory.createBusiness(namespace).isNotNull(errTechMsg, eacEstudio, true);
				
			return eacEstudio;
		 }
			
			});
			
			} catch (DataAccessException e) {
				throw new DomainException(namespace, Type.TECHNICAL, errTechMsg, e);
			}*/
				 
		
	}

	@Override
	public List<EacEstudio> getListEacEstudio() {
		final String namespace = CommonHelper.createNamespace(getClass(), "getEacEstudio");
		final String errTechMsg = "Valor de Construcción y Depreciación no se pueden cargar";
		
		 // Prueba sin conexión a la Base de Datos		
		List<EacEstudio> lEac = new ArrayList<EacEstudio>();		
			
		lEac.add(new EacEstudio(1, 1, "A", "Data Prueba A", "A", "28/09/2016", "28/09/2016", "11810994-0",  "11810994-0"));
		lEac.add(new EacEstudio(2, 1, "N", "Data Prueba B", "A", "28/09/2016", "28/09/2016", "11810994-0",  "11810994-0"));
		return lEac;
	}

	@Override
	public EacEstudio createEacEstudio(EacEstudio valorConstDepre) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public EacEstudio updateEacEstudio(EacEstudio valorConstDepre) {
		// TODO Auto-generated method stub
		return null;
	}	
	

} // Fin Clase
