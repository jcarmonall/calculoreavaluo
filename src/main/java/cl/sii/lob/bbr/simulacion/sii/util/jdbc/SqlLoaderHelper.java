package cl.sii.lob.bbr.simulacion.sii.util.jdbc;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;

import java.util.HashMap;

public class SqlLoaderHelper {
    public static final String EXTENSION = "sqls";
    private HashMap<String, String> sqls = new HashMap<String, String>();

    public SqlLoaderHelper(Class<?> clazz) {
        this(getSqlFilePath(clazz));
    }

    public SqlLoaderHelper(String path) {
        InputStream sqlsInputStream = Thread.currentThread().getContextClassLoader().getResourceAsStream(path);

        if (sqlsInputStream != null) {
            BufferedReader reader = null;

            try {
                reader = new BufferedReader(new InputStreamReader(sqlsInputStream));

                String key = null;
                StringBuilder sql = new StringBuilder();

                for (String line = reader.readLine(); line != null; line = reader.readLine()) {
                    if (line.startsWith("#")) {
                        if (key != null) {
                            sqls.put(key, sql.toString());
                            sql = new StringBuilder();
                        }

                        key = line.replaceAll("#", "");
                    } else {
                        sql.append(line + "\n");
                    }
                }

                if (key != null) {
                    sqls.put(key, sql.toString());
                    sql = new StringBuilder();
                }
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                try {
                    if (reader != null) {
                        reader.close();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private static String getSqlFilePath(Class<?> clazz) {
        String folder = clazz.getPackage().getName().replace(".", "/");
        String fileName = clazz.getSimpleName() + "." + EXTENSION;

        return folder + "/" + fileName;
    }

    public String getSql(String key) {
        return sqls.get(key);
    }
}
