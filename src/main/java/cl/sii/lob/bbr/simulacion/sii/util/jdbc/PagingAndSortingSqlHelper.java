package cl.sii.lob.bbr.simulacion.sii.util.jdbc;

import java.util.List;
import java.util.Map;

//@Component("pagingAndSortingSqlHelper.java")
public class PagingAndSortingSqlHelper<T> {
	
	private static String DIRECTION_ASC="ASC";
	private static String DIRECTION_DESC="DESC";
	
	private String sqlSelect;
	private int pageIndex;
	private int pageSize;
	private int fullSize;
	
	private Map<Integer, String> sorts;
	
	private List<T> listPage;
	
	public PagingAndSortingSqlHelper(){
	}
	
	public PagingAndSortingSqlHelper(String sqlSelect, int pageIndex, int pageSize){
		this.sqlSelect = sqlSelect;
		this.pageIndex = pageIndex;
		this.pageSize = pageSize;
	}
	public PagingAndSortingSqlHelper(String sqlSelect, int pageIndex, int pageSize, Map<Integer, String> sorts){
		this.sqlSelect = sqlSelect;
		this.pageIndex = pageIndex;
		this.pageSize = pageSize;
		this.sorts = sorts;
	}
	
	public void setFullSize(int fullSize){
		this.fullSize = fullSize;
	}
	
	public int getFullSize(){
		return this.fullSize;
	}
	
		
	public int getLastPageIndex(){
		int resto = this.fullSize % this.pageSize;
		int lastPageIndex = 0;
		if(resto == 0)
			lastPageIndex = (this.fullSize / this.pageSize);
		else 
			lastPageIndex = Math.round(this.fullSize / this.pageSize) + 1;
		
		return lastPageIndex;
	}
	
	public int getInitRow(){
		int result = ((this.pageSize * this.pageIndex) - this.pageSize) + 1 ;
		return result;
	}
	
	public int getEndRow(){
		int result = this.pageSize * this.pageIndex;
		if(result > this.fullSize){
			result = this.fullSize;
		}
		return result;
	}
	
	public String getSortByPositionsColumns (){
		Map<Integer, String>  sorts = this.sorts;
		StringBuffer buff = new StringBuffer(100);
		if(sorts.isEmpty() || sorts==null){
			buff.append("1 " );
			buff.append(DIRECTION_DESC);
		}else{
		for(Map.Entry<Integer, String> entry : sorts.entrySet()){
			buff.append(entry.getKey() + " " );
			buff.append(entry.getValue().equalsIgnoreCase(DIRECTION_DESC)?DIRECTION_DESC:DIRECTION_ASC);
			buff.append(",");
			}
			buff.deleteCharAt(buff.length()-1);
		}
		return buff.toString();
	}
	

	
	public String getSqlByPage(){
		StringBuffer buff = new StringBuffer(100);
		buff.append("SELECT * FROM ( SELECT /*+ FIRST_ROWS(n) */ A.*, ROWNUM RNUM FROM ( ");
		buff.append(sqlSelect);
		buff.append(" ) A WHERE ROWNUM <= " );
		buff.append(this.getEndRow());
		buff.append(" ) WHERE RNUM  >=  ");
		buff.append(this.getInitRow());
		
		return buff.toString();
	}
	
	public String getSqlByPageSort(){
		StringBuffer buff = new StringBuffer(100);
		buff.append("SELECT * FROM ( SELECT /*+ FIRST_ROWS(n) */ A.*, ROWNUM RNUM FROM ( ");
		buff.append("SELECT * FROM ( ");
		buff.append(sqlSelect);
		buff.append(") order by ");
		buff.append(this.getSortByPositionsColumns());
		buff.append(" ) A WHERE ROWNUM <= " );
		buff.append(this.getEndRow());
		buff.append(" ) WHERE RNUM  >=  ");
		buff.append(this.getInitRow());
		
		return buff.toString();
	}
	
	public String getSqlBySort(){
		StringBuffer buff = new StringBuffer(100);
		buff.append("SELECT * FROM ( ");
		buff.append(sqlSelect);
		buff.append(") order by ");
		buff.append(this.getSortByPositionsColumns());
		
		return buff.toString();
	}
	
	public String getSqlCount(){
		StringBuffer buff = new StringBuffer(100);
		buff.append("select count(1) from ( ");
		buff.append(this.sqlSelect);
		buff.append(" ) t1 ");
		return buff.toString();
	}

	public List<T> getListPage() {
		return listPage;
	}

	public void setListPage(List<T> listPage) {
		this.listPage = listPage;
	}

	public int getPageIndex() {
		return pageIndex;
	}

	public void setPageIndex(int pageIndex) {
		this.pageIndex = pageIndex;
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public void setSqlSelect(String sqlSelect) {
		this.sqlSelect = sqlSelect;
	}

	public Map<Integer, String> getSorts() {
		return sorts;
	}

	public void setSorts(Map<Integer, String> sorts) {
		this.sorts = sorts;
	}

	public static class Sort {
    	
        private int column;
        private String direction;
        
		public int getColumn() {
			return column;
		}
		public void setColumn(int column) {
			this.column = column;
		}
		public String getDirection() {
			return direction;
		}
		public void setDirection(String direction) {
			this.direction = direction;
		}
    }
	
}

