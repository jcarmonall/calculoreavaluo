/**
 * 
 */
package cl.sii.lob.bbr.simulacion.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.sql.DataSource;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import cl.sii.lob.bbr.simulacion.sii.util.jdbc.SqlLoaderHelper;
import cl.sii.lob.bbr.simulacion.util.exception.CommonHelper;
import cl.sii.lob.bbr.simulacion.util.exception.DomainException;
import cl.sii.lob.bbr.simulacion.util.exception.DomainException.Type;
import cl.sii.lob.bbr.simulacion.util.exception.DomainValidator;
import cl.sii.sdi.lob.bbrr.simulacion.interfaces.ValorConstDepreFactory;
import cl.sii.sdi.lob.bbrr.simulacion.model.ValorConstDepre;
import cl.sii.sdi.lob.bbrr.simulacion.model.ValorConstDepreRepository;



/**
 * @author developer
 *
 */
@Repository("valorConstDepreRepository")
public class ValorConstDepreDaoJdbc implements ValorConstDepreRepository{
	
	 private SqlLoaderHelper sqls = new SqlLoaderHelper(this.getClass());
	 private JdbcTemplate jdbcTemplate;
	 private ValorConstDepreFactory valorConstDepreFactory;
	 
	 @Autowired
	 @Qualifier("valorConstDepreDataSource")
	 public void setDataSource(DataSource dataSource) {
		 this.jdbcTemplate = new JdbcTemplate(dataSource);
		 }
	 
	  @Autowired
	  @Qualifier("valorConstDepreFactory")
	  public void setMessageFactory(ValorConstDepreFactory valorConstDepreFactory) {
		 this.valorConstDepreFactory = valorConstDepreFactory;
		 }
	

	@Override
	public ValorConstDepre getValorConstDepre(float id) {
		final String namespace = CommonHelper.createNamespace(getClass(), "getValorConstDepre");
		final String errTechMsg = "Valor de Construcción y Depreciación no se pueden cargar";
		
		try {
			return jdbcTemplate.queryForObject(sqls.getSql("getValorConstDepre"), new Object[]{id}, new RowMapper<ValorConstDepre>() {
				@Override
				public ValorConstDepre mapRow(ResultSet rs, int index) throws SQLException {
					
					// Validamos que el resultset tenga registros
					float valorContDepreID = rs.getFloat("VALOR_CONST_DEP_ID");
					float eacEstudioId = rs.getFloat("EAC_ESTUDIO_ID");
					String clase = rs.getString("CLASE");
					int calidad = rs.getInt("CALIDAD"); 
					String descripcion = rs.getString("DESCRIPCION"); 
					double tasaDep = rs.getDouble("TASA_DEP"); 
					double porcMaxDep = rs.getDouble("PORC_MAX_DEP");
					double valorRef = rs.getDouble("VALOR_DEF"); 
					double valorProp = rs.getDouble("VALOR_PROP"); 
					String fCrea = rs.getString("F_CREA"); 
					String fAct = rs.getString("F_ACT");
					String rutCrea = rs.getString("RUT_CREA"); 
					String rutAct = rs.getString("RUT_ACT");
					
					ValorConstDepre valorConstDepre = valorConstDepreFactory.create(valorContDepreID, 
																					eacEstudioId, 
																					clase, 
																					calidad, 
																					descripcion, 
																					tasaDep, 
																					porcMaxDep, 
																					valorRef, 
																					valorProp, 
																					fCrea, 
																					fAct, 
																					rutCrea, 
																					rutAct);
					DomainValidator.Factory.createBusiness(namespace).isNotNull(errTechMsg, valorConstDepre, true);
					
					return valorConstDepre ;
				}
			});
		} catch (DataAccessException e) {
			throw new DomainException(namespace, Type.TECHNICAL, errTechMsg, e);
		}	
	}

	@Override
	public List<ValorConstDepre> getValoresConstDepre() {
		final String namespace = CommonHelper.createNamespace(getClass(), "getMessages");
	    final String errTechMsg = "Valores de Construcción y Depreciación no se pueden cargar";
	    
	    // Prueba sin conexión a la Base de Datos		
		List<ValorConstDepre> lvcd = new ArrayList<ValorConstDepre>();		
	    float valorContDepreID = 1;
		float eacEstudioId = 1;
		String clase = "A";
		int calidad = 1;
		String descripcion = "Prueba";
		double tasaDep = 1.01;
		double porcMaxDep = 2.10;
		double valorRef = 3.01; 
		double valorProp = 4.01; 
		String fCrea =   DateTimeZone.getDefault().toString();
		String fAct = DateTimeZone.getDefault().toString();
		String rutCrea = "1-A";
		String rutAct = "1-B";
		ValorConstDepre e = new ValorConstDepre(valorContDepreID, 
												eacEstudioId, 
												clase, 
												calidad, 
												descripcion, 
												tasaDep, 
												porcMaxDep, 
												valorRef, 
												valorProp, 
												fCrea, 
												fAct, 
												rutCrea, 
												rutAct);
		
		lvcd.add(e);
		return lvcd;
	    
	    
	    /*
			try {				
				return jdbcTemplate.query(sqls.getSql("getvaloresConstDep"), new RowMapper<ValorConstDepre>() {					
					
					@Override
					public ValorConstDepre mapRow(ResultSet rs, int index) throws SQLException {
						// Validamos que el resultset tenga registros
						DomainValidator.Factory.createBusiness(namespace).isTrue(errTechMsg, rs.getRow() > 0, true);
						
						float valorContDepreID = rs.getFloat("VALOR_CONST_DEP_ID");
						float eacEstudioId = rs.getFloat("EAC_ESTUDIO_ID");
						String clase = rs.getString("CLASE");
						int calidad = rs.getInt("CALIDAD"); 
						String descripcion = rs.getString("DESCRIPCION"); 
						double tasaDep = rs.getDouble("TASA_DEP"); 
						double porcMaxDep = rs.getDouble("PORC_MAX_DEP");
						double valorRef = rs.getDouble("VALOR_DEF"); 
						double valorProp = rs.getDouble("VALOR_PROP"); 
						Timestamp fCrea = rs.getTimestamp("F_CREA"); 
						Timestamp fAct = rs.getTimestamp("F_ACT");
						String rutCrea = rs.getString("RUT_CREA"); 
						String rutAct = rs.getString("RUT_ACT");
						
						return valorConstDepreFactory.create(valorContDepreID, 
															 eacEstudioId, 
															 clase, 
															 calidad, 
															 descripcion, 
															 tasaDep,
															 porcMaxDep, 
															 valorRef, 
															 valorProp, 
															 fCrea, 
															 fAct, 
															 rutCrea, 
															 rutAct);
					}
				});
			} catch (DataAccessException e) {
				throw new DomainException(namespace, Type.TECHNICAL, errTechMsg, e);
			}*/
	}

	@Override
	public ValorConstDepre createValorConstDepre(ValorConstDepre valorConstDepre) {
		final String namespace = CommonHelper.createNamespace(getClass(), "createValorConstDepre");
	    final String errTechMsg = "Valor de Construcción y Depreciación no se pueden guardar";
			try {
				int result = jdbcTemplate.update(sqls.getSql("createValorConstDepre"), 
						new Object[]{valorConstDepre.getValorContDepreID() ,
					                 valorConstDepre.getEacEstudioId(),
					                 valorConstDepre.getClase(),
					                 valorConstDepre.getCalidad(),
					                 valorConstDepre.getDescripcion(),
					                 valorConstDepre.getTasaDep(),
					                 valorConstDepre.getPorcMaxDep(),
					                 valorConstDepre.getValorRef(),
					                 valorConstDepre.getValorProp(),
					                 valorConstDepre.getfCrea(),
					                 valorConstDepre.getfAct(),
					                 valorConstDepre.getRutCrea(),
					                 valorConstDepre.getRutAct()});
				if (result == 1){
					return valorConstDepre;
				}else{
					throw new DomainException(namespace, Type.TECHNICAL, errTechMsg, new Exception("No se insertï¿œ informaciï¿œn"));
				}
			} catch (DataAccessException e) {
				throw new DomainException(namespace, Type.TECHNICAL, errTechMsg, e);
			}
	}

	@Override
	public ValorConstDepre updateValorConstDepre(ValorConstDepre valorConstDepre) {
		final String namespace = CommonHelper.createNamespace(getClass(), "updateValorConstDepre");
		final String errTechMsg = "Message no se puede actualizar";
		try {
			int result = jdbcTemplate.update(sqls.getSql("updateValorConstDepre"), 
					new Object[]{valorConstDepre.getValorContDepreID(),
                				 valorConstDepre.getEacEstudioId(),
                				 valorConstDepre.getClase(),
                				 valorConstDepre.getCalidad(),
                				 valorConstDepre.getDescripcion(),
                				 valorConstDepre.getTasaDep(),
                				 valorConstDepre.getPorcMaxDep(),
                				 valorConstDepre.getValorRef(),
                				 valorConstDepre.getValorProp(),
                				 valorConstDepre.getfCrea(),
                				 valorConstDepre.getfAct(),
                				 valorConstDepre.getRutCrea(),
                				 valorConstDepre.getRutAct()});
			if (result == 1){
				return valorConstDepre;
			}else{
				throw new DomainException(namespace, Type.TECHNICAL, errTechMsg, new Exception("No se actualizï¿œ informaciï¿œn"));
			}
		} catch (DataAccessException e) {
			throw new DomainException(namespace, Type.TECHNICAL, errTechMsg, e);
		}
	}

}