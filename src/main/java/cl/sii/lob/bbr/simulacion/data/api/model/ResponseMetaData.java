package cl.sii.lob.bbr.simulacion.data.api.model;

import java.util.Arrays;
import java.util.List;

public class ResponseMetaData extends MetaData {
    private static final long serialVersionUID = 1L;
    private List<Message> info;
    private List<Message> errors;
    private Page page;

    public Message[] getInfo() {
        return info == null ? null : info.toArray(new Message[] {});
    }

    public void setInfo(Message[] info) {
        if (info != null) {
            this.info = Arrays.asList(info);
        }
    }

    public Message[] getErrors() {
        return errors == null ? null : errors.toArray(new Message[] {});
    }

    public void setErrors(Message[] errors) {
        if (errors != null) {
            this.errors = Arrays.asList(errors);
        }
    }

    public Page getPage() {
        return page;
    }

    public void setPage(Page page) {
        this.page = page;
    }

    public static class Page {
        private int fullSize;
        private int currentPageIndex;
        private int pageSize;
        private int lastPageIndex;

        public int getFullSize() {
            return fullSize;
        }

        public void setFullSize(int fullSize) {
            this.fullSize = fullSize;
        }

        public int getCurrentPageIndex() {
            return currentPageIndex;
        }

        public void setCurrentPageIndex(int currentPageIndex) {
            this.currentPageIndex = currentPageIndex;
        }

        public int getPageSize() {
            return pageSize;
        }

        public void setPageSize(int pageSize) {
            this.pageSize = pageSize;
        }

        public int getLastPageIndex() {
            return lastPageIndex;
        }

        public void setLastPageIndex(int lastPageIndex) {
            this.lastPageIndex = lastPageIndex;
        }
    }

}
