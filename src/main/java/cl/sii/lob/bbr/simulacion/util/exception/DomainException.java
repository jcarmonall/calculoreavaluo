package cl.sii.lob.bbr.simulacion.util.exception;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import cl.sii.lob.bbr.simulacion.util.exception.DomainException.DomainExceptionElement.Factory;



public class DomainException extends RuntimeException {
    private static final long serialVersionUID = 1L;
    private List<DomainExceptionElement> elements;

    public DomainException(DomainException exception) {
        exception.iterate(new Callback() {
            @Override
            public void processElement(DomainExceptionElement element, int index) {
                add(element);
            }
        });
    }

    public DomainException(DomainExceptionElement element) {
        add(element);
    }

    public DomainException(final String namespace, final Type type, final String message) {
        this(Factory.create(namespace, type, message));
    }

    public DomainException(final String namespace, final Type type, final String message, final Throwable t) {
        this(Factory.create(namespace, type, message, t));
    }

    public DomainException(List<DomainExceptionElement> elements) {
        this.elements = elements;
    }

    public DomainException add(DomainExceptionElement exception) {
        if (elements == null) {
            elements = new ArrayList<DomainExceptionElement>();
        }
        elements.add(exception);
        return this;
    }

    public void iterate(Callback callback) {
        if (elements != null) {
            int index = 0;
            for (DomainExceptionElement element : elements) {
                callback.processElement(element, index++);
            }
        }
    }

    @Override
    public String getMessage() {
        if (elements != null && elements.size() > 0) {
            return elements.get(elements.size() - 1).getMessage();
        }
        return null;
    }

    public Type getType() {
        if (elements != null && elements.size() > 0) {
            return elements.get(elements.size() - 1).getType();
        }
        return null;
    }

    public int getSize() {
        if (elements != null && elements.size() > 0) {
            return elements.size();
        }
        return 0;
    }

    public static interface Callback {
        void processElement(DomainExceptionElement element, int index);
    }

    public static interface DomainExceptionElement {
        public Date getDate();

        public String getNamespace();

        public Type getType();

        public String getMessage();

        public ClassType getClassType();

        public Throwable getThrowable();

        public static class Factory {
            public static DomainExceptionElement create(final String namespace, final Type type, final String message) {
                return create(namespace, type, message, null);
            }

            public static DomainExceptionElement create(final String namespace, final Type type, final String message, final Throwable t) {
                final ClassType domainType = Helper.translateToDomainType(namespace);
                return new DomainExceptionElement() {

                    @Override
                    public Date getDate() {
                        return new Date();
                    }

                    @Override
                    public String getNamespace() {
                        return namespace;
                    }

                    @Override
                    public Type getType() {
                        return type;
                    }

                    @Override
                    public String getMessage() {
                        return message;
                    }

                    @Override
                    public ClassType getClassType() {
                        return domainType;
                    }

                    @Override
                    public Throwable getThrowable() {
                        return t;
                    }
                };
            }
        }
    }

    public static enum Type {
        BUSINESS, TECHNICAL, UNKNOWN;
    }

    public static enum ClassType {
        BO, ACL, IL, API, AS, OTHER;
    }

    public static class Helper {
        public static ClassType translateToDomainType(String namespace) {
            if (namespace.contains(".bo.")) {
                return ClassType.BO;
            } else if (namespace.contains(".acl.")) {
                return ClassType.ACL;
            } else if (namespace.contains(".il.")) {
                return ClassType.IL;
            } else if (namespace.contains(".api.")) {
                return ClassType.API;
            } else if (namespace.contains("data.impl.") || namespace.contains("ui.impl.")) {
                return ClassType.AS;
            }
            return ClassType.OTHER;
        }
    }
}
