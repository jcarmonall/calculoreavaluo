package cl.sii.lob.bbr.simulacion.data.api.model;

import java.io.Serializable;

public abstract class MetaData implements Serializable {
    private static final long serialVersionUID = 1L;

    private String conversationId;
    private String transactionId;
    private String namespace;

    public String getConversationId() {
        return conversationId;
    }

    public void setConversationId(String conversationId) {
        this.conversationId = conversationId;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public String getNamespace() {
        return namespace;
    }

    public void setNamespace(String namespace) {
        this.namespace = namespace;
    }

    public static class Message {
        private String id;
        private String descripcion;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getDescripcion() {
            return descripcion;
        }

        public void setDescripcion(String descripcion) {
            this.descripcion = descripcion;
        }

    }
}
