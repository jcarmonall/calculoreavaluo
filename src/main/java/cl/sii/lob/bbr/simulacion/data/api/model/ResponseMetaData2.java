package cl.sii.lob.bbr.simulacion.data.api.model;

import java.util.Arrays;
import java.util.List;

public class ResponseMetaData2 extends MetaData2 {
    private static final long serialVersionUID = 1L;
    private List<Message2> info;
    private List<Message2> errors;
    private ResponsePage page;
    private List<Sort> sorts;

    public Message2[] getInfo() {
        return info == null ? null : info.toArray(new Message2[] {});
    }

    public void setInfo(Message2[] info) {
        if (info != null) {
            this.info = Arrays.asList(info);
        }
    }

    public Message2[] getErrors() {
        return errors == null ? null : errors.toArray(new Message2[] {});
    }

    public void setErrors(Message2[] errors) {
        if (errors != null) {
            this.errors = Arrays.asList(errors);
        }
    }

    public ResponsePage getPage() {
        return page;
    }

    public void setPage(ResponsePage page) {
        this.page = page;
    }
    
    public Sort[] getSorts() {
        return sorts == null ? null : sorts.toArray(new Sort[] {});
    }
    
    public void setSorts(Sort[] sorts) {
        if (sorts != null) {
            this.sorts = Arrays.asList(sorts);
        }
    }

    public static class ResponsePage {
        private int fullSize;
        private int currentPageIndex;
        private int pageSize;
        private int lastPageIndex;

        public int getFullSize() {
            return fullSize;
        }

        public void setFullSize(int fullSize) {
            this.fullSize = fullSize;
        }

        public int getCurrentPageIndex() {
            return currentPageIndex;
        }

        public void setCurrentPageIndex(int currentPageIndex) {
            this.currentPageIndex = currentPageIndex;
        }

        public int getPageSize() {
            return pageSize;
        }

        public void setPageSize(int pageSize) {
            this.pageSize = pageSize;
        }

        public int getLastPageIndex() {
            return lastPageIndex;
        }

        public void setLastPageIndex(int lastPageIndex) {
            this.lastPageIndex = lastPageIndex;
        }

    }

}
