package cl.sii.lob.bbr.simulacion.util.exception;

import java.util.ArrayList;
import java.util.List;

import cl.sii.lob.bbr.simulacion.util.exception.DomainException.DomainExceptionElement;
import cl.sii.lob.bbr.simulacion.util.exception.DomainException.Type;

public class DomainValidator {
    private String namespace;
    private Type type;
    private List<DomainExceptionElement> elements;

    public DomainValidator(String namespace, Type type) {
        this.namespace = namespace;
        this.type = type;
    }

    public void isNotNull(String message, Object actual, boolean raiseIfFailed) {
        if (actual == null) {
            add(DomainExceptionElement.Factory.create(namespace, type, message));
        }
        if (raiseIfFailed) {
            raiseIfFailed();
        }
    }

    public void isNotEmpty(String message, String actual, boolean raiseIfFailed) {
        if (actual == null || actual.trim().length() == 0) {
            add(DomainExceptionElement.Factory.create(namespace, type, message));
        }
        if (raiseIfFailed) {
            raiseIfFailed();
        }
    }

    public void isListEmpty(String message, List actual, boolean raiseIfFailed) {
        if (actual != null && actual.size() > 0) {
            add(DomainExceptionElement.Factory.create(namespace, type, message));
        }
        if (raiseIfFailed) {
            raiseIfFailed();
        }
    }
    
    public void isListNotEmpty(String message, List actual, boolean raiseIfFailed) {
        if (actual == null || actual.size() == 0) {
            add(DomainExceptionElement.Factory.create(namespace, type, message));
        }
        if (raiseIfFailed) {
            raiseIfFailed();
        }
    }
    
    public void isEquals(String message, Object expected, Object actual, boolean raiseIfFailed) {
        if (expected != null && actual != null) {
            if (!expected.equals(actual)) {
                add(DomainExceptionElement.Factory.create(namespace, type, message));
            }
        } else {
            add(DomainExceptionElement.Factory.create(namespace, type, message));
        }
        if (raiseIfFailed) {
            raiseIfFailed();
        }
    }

    public void isTrue(String message, boolean actual, boolean raiseIfFailed) {
        if (!actual) {
            add(DomainExceptionElement.Factory.create(namespace, type, message));
        }
        if (raiseIfFailed) {
            raiseIfFailed();
        }
    }

    public void isFalse(String message, boolean actual, boolean raiseIfFailed) {
        if (actual) {
            add(DomainExceptionElement.Factory.create(namespace, type, message));
        }
        if (raiseIfFailed) {
            raiseIfFailed();
        }
    }

    public void isMatch(String message, String actual, String regex, boolean raiseIfFailed) {
        if (actual == null || !actual.matches(regex)) {
            add(DomainExceptionElement.Factory.create(namespace, type, message));
        }
        if (raiseIfFailed) {
            raiseIfFailed();
        }
    }

    public void raiseIfFailed() {
        if (elements != null) {
            throw new DomainException(elements);
        }
    }

    public void add(DomainExceptionElement element) {
        if (elements == null) {
            elements = new ArrayList<DomainExceptionElement>();
        }
        elements.add(element);
    }

    public int getSize() {
        if (elements != null && elements.size() > 0) {
            return elements.size();
        }
        return 0;
    }

    public String getLastMessage() {
        if (elements != null && elements.size() > 0) {
            return elements.get(elements.size() - 1).getMessage();
        }
        return null;
    }

    public static class Factory {

        public static DomainValidator createBusiness(Class<?> clazz, String operation) {
            return createBusiness(CommonHelper.createNamespace(clazz, operation));
        }

        public static DomainValidator createTechnical(Class<?> clazz, String operation) {
            return createTechnical(CommonHelper.createNamespace(clazz, operation));
        }

        public static DomainValidator createUnknown(Class<?> clazz, String operation) {
            return createUnknown(CommonHelper.createNamespace(clazz, operation));
        }

        public static DomainValidator createBusiness(String namespace) {
            return new DomainValidator(namespace, Type.BUSINESS);
        }

        public static DomainValidator createTechnical(String namespace) {
            return new DomainValidator(namespace, Type.TECHNICAL);
        }

        public static DomainValidator createUnknown(String namespace) {
            return new DomainValidator(namespace, Type.UNKNOWN);
        }

    }

}
