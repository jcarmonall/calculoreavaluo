package cl.sii.lob.bbr.simulacion.data.api.model;

public interface ModelResponse2<T> {
    void setData(T data);

    T getData();

    void setMetaData(ResponseMetaData2 responseMetaData);

    ResponseMetaData2 getMetaData();
}
