package cl.sii.lob.bbr.simulacion.util.exception;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;

import cl.sii.lob.bbr.simulacion.data.api.model.ResponseMetaData;
import cl.sii.lob.bbr.simulacion.data.api.model.ResponseMetaData2;
import cl.sii.lob.bbr.simulacion.util.exception.DomainException.Callback;
import cl.sii.lob.bbr.simulacion.util.exception.DomainException.DomainExceptionElement;
import cl.sii.lob.bbr.simulacion.util.exception.DomainException.Type;



@Service("incidentDataService")
public class IncidentDataService {
    private static final Log LOG = LogFactory.getLog(IncidentDataService.class);

    public void raise(ResponseMetaData responseMetaData, DomainException e) {
        final String incidentId = responseMetaData.getConversationId() + "/" + responseMetaData.getTransactionId();
        e.iterate(new Callback() {
            @Override
            public void processElement(DomainExceptionElement element, int index) {
                String namespace = element.getNamespace();
                Type type = element.getType();
                String msg = element.getMessage();
                String logMsg = incidentId + "/" + type.name() + "/" + namespace + "|" + msg;
                Throwable t = element.getThrowable();
                if (type == Type.BUSINESS) {
                    LOG.warn(logMsg);
                } else {
                    LOG.error(logMsg, t);
                }
            }
        });
    }

    public void raise2(ResponseMetaData2 responseMetaData, DomainException e) {
        final String incidentId = responseMetaData.getConversationId() + "/" + responseMetaData.getTransactionId();
        e.iterate(new Callback() {
            @Override
            public void processElement(DomainExceptionElement element, int index) {
                String namespace = element.getNamespace();
                Type type = element.getType();
                String msg = element.getMessage();
                String logMsg = incidentId + "/" + type.name() + "/" + namespace + "|" + msg;
                Throwable t = element.getThrowable();
                if (type == Type.BUSINESS) {
                    LOG.warn(logMsg);
                } else {
                    LOG.error(logMsg, t);
                }
            }
        });
    }
}
