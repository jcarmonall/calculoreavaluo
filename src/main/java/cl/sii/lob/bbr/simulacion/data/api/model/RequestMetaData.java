package cl.sii.lob.bbr.simulacion.data.api.model;

public class RequestMetaData extends MetaData {
    private static final long serialVersionUID = 1L;
    private Page page;

    public void setPage(Page page) {
        this.page = page;
    }

    public Page getPage() {
        return page;
    }

    public static class Page {
        private int pageSize;
        private int pageIndex;

        public int getPageSize() {
            return pageSize;
        }

        public void setPageSize(int pageSize) {
            this.pageSize = pageSize;
        }

        public int getPageIndex() {
            return pageIndex;
        }

        public void setPageIndex(int pageIndex) {
            this.pageIndex = pageIndex;
        }
    }
}
