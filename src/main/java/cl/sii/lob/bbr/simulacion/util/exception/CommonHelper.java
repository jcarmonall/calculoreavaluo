package cl.sii.lob.bbr.simulacion.util.exception;

import java.text.SimpleDateFormat;
import java.util.Date;

public class CommonHelper {

    public static String createNamespace(Class<?> clazz, String operation) {
        return clazz.getName() + "/" + operation;
    }
    
    
    public static String toYyyyMMdd(Date fecha) {
        return new SimpleDateFormat("yyyyMMdd").format(fecha);
    }
}
