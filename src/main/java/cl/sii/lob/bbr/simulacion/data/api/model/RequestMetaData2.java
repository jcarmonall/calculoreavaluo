package cl.sii.lob.bbr.simulacion.data.api.model;

import java.util.Arrays;
import java.util.List;

public class RequestMetaData2 extends MetaData2 {
    
	private static final long serialVersionUID = 1L;
	private RequestPage page;
	private List<Sort> sorts;

    public void setPage(RequestPage page) {
        this.page = page;
    }

    public RequestPage getPage() {
        return page;
    }

    public Sort[] getSorts() {
        return sorts == null ? null : sorts.toArray(new Sort[] {});
    }
    
    public void setSorts(Sort[] sorts) {
        if (sorts != null) {
            this.sorts = Arrays.asList(sorts);
        }
    }

	public static class RequestPage {
        private int pageSize;
        private int pageIndex;

        public int getPageSize() {
            return pageSize;
        }

        public void setPageSize(int pageSize) {
            this.pageSize = pageSize;
        }

        public int getPageIndex() {
            return pageIndex;
        }

        public void setPageIndex(int pageIndex) {
            this.pageIndex = pageIndex;
        }
    }
	
    
}
