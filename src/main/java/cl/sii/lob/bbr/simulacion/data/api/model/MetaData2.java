package cl.sii.lob.bbr.simulacion.data.api.model;

import java.io.Serializable;

public abstract class MetaData2 implements Serializable {
    private static final long serialVersionUID = 1L;

    private String conversationId;
    private String transactionId;
    private String namespace;

    public String getConversationId() {
        return conversationId;
    }

    public void setConversationId(String conversationId) {
        this.conversationId = conversationId;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public String getNamespace() {
        return namespace;
    }

    public void setNamespace(String namespace) {
        this.namespace = namespace;
    }

    public static class Message2 {
        private String id;
        private String descripcion;
        private String type;
        
        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getDescripcion() {
            return descripcion;
        }

        public void setDescripcion(String descripcion) {
            this.descripcion = descripcion;
        }
        
        public String getType() {
			return type;
		}
        
        public void setType(String type) {
			this.type = type;
		}

    }

    public static class Sort {

        private int sortColumn;
        private String sortDirection;

        public int getSortColumn() {
            return sortColumn;
        }

        public void setSortColumn(int sortColumn) {
            this.sortColumn = sortColumn;
        }

        public String getSortDirection() {
            return sortDirection;
        }

        public void setSortDirection(String sortDirection) {
            this.sortDirection = sortDirection;
        }

    }
}
